from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from ..models import Category
from ..forms import CategoryForm

class IndexView(ListView):
    template_name = 'index.html'
    model = Category
    context_object_name = 'categories'
    paginate_by = 6

class CategoryCreateView(PermissionRequiredMixin, LoginRequiredMixin, CreateView):
    model = Category
    template_name = 'categories/category_create.html'
    form_class = CategoryForm
    success_url = 'home_page'
    permission_required = 'my_shop.add_category'

class CategoryUpdateView(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    model = Category
    template_name = 'categories/category_update.html'
    form_class = CategoryForm
    success_url = 'home_page'
    permission_required = 'my_shop.change_category'

class CategoryDeleteView(PermissionRequiredMixin, LoginRequiredMixin, DeleteView):
    model = Category
    template_name = 'categories/category_delete.html'
    success_url = 'home_page'
    permission_required = 'my_shop.delete_category'