from typing import Any
from django.db.models.query import QuerySet
from django.urls import reverse, reverse_lazy
from ..models import Product, Category
from .. forms import ProductForm
from django.views.generic import ListView, UpdateView, CreateView, DeleteView, DetailView
from django.db.models import *
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin


class ProductListView(ListView):
    template_name = 'products/product_list.html'
    model = Product
    context_object_name = 'products'
    paginate_by = 6


class ProductCategoryView(ProductListView):
    def get_queryset(self) -> QuerySet[Any]:
        pk = self.kwargs.get('pk')
        category = Category.objects.get(pk=pk)
        products = category.products.all()
        return products

class ProductDetailView(DetailView):
    model = Product
    template_name = 'products/product_details.html'
    context_object_name = 'product'

class ProductCreateView(PermissionRequiredMixin,LoginRequiredMixin, CreateView):
    model = Product
    template_name = 'products/product_create.html'
    form_class = ProductForm
    permission_required = 'my_shop.add_product'
    

class ProductDeleteView(PermissionRequiredMixin, LoginRequiredMixin, DeleteView):
    model = Product
    template_name = 'products/product_delete.html'
    success_url = reverse_lazy('product_list_all')
    permission_required = 'my_shop.delete_product'

    def get_success_url(self) -> str:
        next = self.request.GET.get('next')
        if not next:
            next = self.request.POST.get('next')
        if not next:
            next = reverse('product_list_all')
        return next
    
class ProductUpdateView(PermissionRequiredMixin, LoginRequiredMixin, UpdateView):
    model = Product
    template_name = 'products/product_update.html'
    form_class = ProductForm
    permission_required = 'my_shop.change_product'
    
    def get_success_url(self) -> str:
        next = self.request.GET.get('next')
        if not next:
            next = self.request.POST.get('next')
        print(next)
        return next