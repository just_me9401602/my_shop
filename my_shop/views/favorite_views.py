from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from ..models import Product
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

@login_required
@csrf_exempt
def add_to_favorite(request):
    if request.method == 'POST':
        product_id = request.POST.get('product_id')
        print(product_id)
        product = get_object_or_404(Product, pk=product_id)
        if product in request.user.profile.favorites.all():
            request.user.profile.favorites.remove(product)
            response_data = {'is_favorite': False, 'success': True}
        else:
            request.user.profile.favorites.add(product)
            response_data = {'is_favorite': True, 'success': True}
        return JsonResponse(response_data)


class FavoriteListView(LoginRequiredMixin, ListView):
    model = Product
    template_name = 'products/favorite_list.html'
    context_object_name = 'products'

    def get_queryset(self):
        return self.model.objects.filter(id__in=self.request.user.profile.favorites.all())