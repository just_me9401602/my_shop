from django import forms
from django.forms import widgets
from django.urls import reverse
from .models import Category
from .models import Product

# class CategoryForm(forms.Form):
#     name = forms.CharField(
#         max_length=50,
#         required=True,
#         label='Название категории',
#         widget=widgets.TextInput(attrs={
#             'class': 'form-control',
#             'placeholder': 'Категория'
#         })  
#     )
#     description = forms.CharField(
#         max_length=100,
#         required=False,
#         label='Описание',
#         widget=widgets.Textarea(attrs={
#             'class': 'form-control',
#             'rows': 2
#         })
#     )
#     picture=forms.CharField(
#         max_length=100,
#         required=False,
#         label='URL картинки',
#         widget=widgets.TextInput(attrs={
#             'class': 'form-control'
#         })
#     )
class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'
        labels = {
            'name': 'Название категории',
            'description': 'Описание',
            'picture': 'URL картинки',
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Категория'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'rows': 4}),
            'picture': forms.TextInput(attrs={'class': 'form-control'}),
        }

class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'
        labels = {
            'category': 'Категория',
            'price': 'Цена',
            'description': 'Описание',
            'picture': 'URL картинки',
            'name': 'Название',
            'seller' : 'Продавец',
            'quontity': 'Количество',
        }
        widgets = {
            'category': forms.Select(attrs={'class': 'form-control'}),
            'price': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 0.00}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'rows': 4}),
            'picture': forms.TextInput(attrs={'class': 'form-control'}),
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Товар'}),
            'seller' : forms.Select(attrs={'class': 'form-control'}),
            'quontity': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 0.00}),
        }
    