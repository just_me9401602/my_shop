from django.db import models
from django.contrib.auth import get_user_model
from django.urls import reverse

class Category(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False, verbose_name='category_name')
    description = models.TextField(max_length=100, null=True, blank=True, verbose_name='description')
    picture = models.CharField(max_length=100, null=True, blank=True, verbose_name='picture')
    def __str__(self):
        return self.name
    

class Product(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False, verbose_name='product_name')
    description = models.TextField(max_length=350, null=True, blank=True, verbose_name='description')
    picture = models.CharField(max_length=100, null=True, blank=True, verbose_name='picture')
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00, verbose_name='price')
    category = models.ForeignKey(to=Category, on_delete=models.CASCADE, related_name='products', null=False, verbose_name='category')
    created_at = models.DateTimeField(auto_now_add = True, verbose_name='created_at')
    quontity = models.IntegerField(null=True, blank=True, verbose_name='Quontity')
    seller = models.ForeignKey(get_user_model(), related_name='products', on_delete = models.CASCADE, null=True, verbose_name='Seller')

    def __str__(self):
        return f'#{self.pk} {self.name}'
    
    def get_absolute_url(self):
        return reverse('product_details', kwargs={'pk': self.pk})
    