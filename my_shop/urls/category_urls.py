from django.urls import path
from my_shop.views.category_views import (
    CategoryCreateView, CategoryUpdateView,
    CategoryDeleteView)


urlpatterns = [
    path('add/', CategoryCreateView.as_view(), name='category_create'),
    path('<int:pk>/edit', CategoryUpdateView.as_view(), name='category_update'),
    path('<int:pk>/delete', CategoryDeleteView.as_view(), name='category_delete')
]