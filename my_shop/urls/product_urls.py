from django.urls import path
from my_shop.views.product_views import (
    ProductCreateView, ProductDetailView, 
    ProductUpdateView, ProductDeleteView,
    ProductListView, ProductCategoryView)


urlpatterns = [
    # path('list/<name>', product_list_view, name='product_list'),
    path('list/', ProductListView.as_view(), name='product_list_all'),
    path('category/<pk>/list/', ProductCategoryView.as_view(), name='product_category_list'),
    path('<int:pk>', ProductDetailView.as_view(), name='product_details'),
    path('add/', ProductCreateView.as_view(), name='product_create'),
    path('<int:pk>/edit', ProductUpdateView.as_view(), name='product_update'),
    path('<int:pk>/delete', ProductDeleteView.as_view(), name='product_delete')
]