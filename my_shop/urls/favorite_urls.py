from django.urls import path
from my_shop.views.favorite_views import add_to_favorite, FavoriteListView

urlpatterns = [
    path('add/', add_to_favorite, name='add_to_favorite'),
    path('list/', FavoriteListView.as_view(), name='favorite_list')
]
