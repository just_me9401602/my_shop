from django.contrib import admin
from .models import Order, OrderItem, StatusOrder, StatusOrderItem, Comment

admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(StatusOrder)
admin.site.register(StatusOrderItem)
admin.site.register(Comment)
