from django.urls import path
from .views import delivery_approve_view, complete_order_view, payment_success_view, payment_fail_view

urlpatterns = [
    path('checkout/', delivery_approve_view, name='delivery_approve'),
    path('complete/', complete_order_view, name='complete_order'),
    path('success/', payment_success_view, name='payment_success'),
    path('fail/', payment_fail_view, name='payment_fail'),
]
