from django.db import models
from django.contrib.auth import get_user_model
from my_shop.models import Product


class StatusOrder(models.Model):
    status = models.CharField(max_length=100, null=False, blank=False, verbose_name='status')

    def __str__(self):
        return self.status

class StatusOrderItem(models.Model):
    status = models.CharField(max_length=100, null=False, blank=False, verbose_name='status')

    def __str__(self):
        return self.status

class Order(models.Model):
    status = models.ForeignKey(StatusOrder, on_delete=models.PROTECT, related_name='orders', verbose_name='Status')
    total = models.DecimalField(max_digits=10, decimal_places=2, default=0.00, verbose_name='Total')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created at')
    buyer= models.ForeignKey(get_user_model(), on_delete=models.PROTECT, related_name='orders', verbose_name='Buyer')
    def __str__(self):
        return f'Ордер #{self.pk}; Статус: {self.status.status}'

class OrderItem(models.Model):
    seller = models.ForeignKey(get_user_model(), on_delete=models.PROTECT, related_name='order_items', verbose_name='Seller')
    product = models.ForeignKey(Product, on_delete=models.PROTECT, related_name='order_items', verbose_name='Product')
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_items', verbose_name='Order')
    quontity = models.IntegerField(null=False, blank=False, verbose_name='Quontity')
    total_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00, verbose_name='Total price')
    status = models.ForeignKey(StatusOrderItem, on_delete=models.PROTECT, related_name='order_items', verbose_name='Status')

    def __str__(self):
        return f'{self.product.name}; Ордер #{self.order.pk}; Статус: {self.status.status}'
    
class Comment(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT, related_name='comments', verbose_name='User')
    text = models.TextField(max_length=1000, null=False, blank=False, verbose_name='Text')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Updated at')
    order_item = models.ForeignKey(OrderItem, on_delete=models.CASCADE, related_name='comments', verbose_name='Order item')

    def __str__(self):
        return self.text