from django.shortcuts import render
from django.views.generic import UpdateView
from accounts.models import Delivery
from accounts.forms import DeliveryEditForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from cart.cart import Cart
from .models import Order, OrderItem, StatusOrder, StatusOrderItem, Comment
from my_shop.models import Product
from django.contrib.auth import get_user_model
from decimal import Decimal


def delivery_approve_view(request):
    if request.method == 'GET':
        form = DeliveryEditForm(instance=Delivery.objects.get(user=request.user))
        return render(request, 'payments/delivery_approve.html', context={'form': form})

def complete_order_view(request):
    if request.method == 'POST':
        delivery = Delivery.objects.get(user=request.user)
        delivery.city = request.POST.get('city')
        delivery.street = request.POST.get('street')
        delivery.home = request.POST.get('home')
        delivery.appart = request.POST.get('appart')
        delivery.save()

        cart_class = Cart(request)
        cart = cart_class.cart
        product_ids = cart.keys()

        order = Order.objects.create(status=StatusOrder.objects.get(status='Открыт'), total=cart_class.get_total_price(), buyer=request.user)

        for product_id in product_ids:
            product = Product.objects.get(id=product_id)
            seller = get_user_model().objects.get(id=product.seller.id)
            quontity = cart[product_id]['quantity']
            price = cart[product_id]['price']
            total_price = quontity * Decimal(price)

            instance = OrderItem.objects.create(
                order=order,
                product=product,
                seller=seller,
                quontity=quontity,
                total_price=total_price,
                status = StatusOrderItem.objects.get(status='В обработке')
            )
            product.quontity -= quontity
            product.save()
            if cart[product_id]['comment'] != '':
                comment =Comment.objects.create(user=request.user, text=cart[product_id]['comment'], order_item=instance)
            
        return JsonResponse({'status': 'success'})

def payment_success_view(request):
    cart = Cart(request)
    cart.clear()
    return render(request, 'payments/payment_success.html')

def payment_fail_view(request):
    return render(request, 'payments/payment_fail.html')

