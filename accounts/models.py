from django.db import models
from django.contrib.auth import get_user_model
from my_shop.models import Product


class Delivery(models.Model):
    user = models.OneToOneField(
        get_user_model(),
        related_name = 'delivery',
        on_delete = models.CASCADE,
        verbose_name = 'Пользователь' 
        )
    city = models.CharField(max_length=100, null=True, blank=True, verbose_name='City')
    street = models.CharField(max_length=100, null=True, blank=True, verbose_name='Street')
    home = models.CharField(max_length=100, null=True, blank=True, verbose_name='Home')
    appart = models.CharField(max_length=100, null=True, blank=True, verbose_name='Appart')
    
    def __str__(self) -> str:
        return f'Город: {self.city}, улица: {self.street}, дом: {self.home}, квартира: {self.appart}'
        
class Profile(models.Model):
    user = models.OneToOneField(
        get_user_model(),
        related_name = 'profile',
        on_delete = models.CASCADE,
        verbose_name = 'Пользователь' 
        )
    phone = models.CharField(max_length=20, null=True, blank=True, verbose_name='Phone')
    avatar = models.ImageField(null=True, blank=True, upload_to='user_pics', verbose_name='Avatar')
    favorites = models.ManyToManyField(Product, related_name='profile', verbose_name='Favorite')
    is_approved = models.BooleanField(default=False)

    def __str__(self) -> str:
        return self.user.get_full_name()