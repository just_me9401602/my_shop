from payments.models import Comment, OrderItem, Order, StatusOrderItem
from my_shop.models import Product
from accounts.models import Profile
from django.contrib.auth import get_user_model
from rest_framework import serializers

class CommentSerializer(serializers.ModelSerializer):
    # user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    class Meta:
        model = Comment
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id','username', 'first_name', 'last_name', 'email']

class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    class Meta:
        model = Profile
        fields = ["id","phone", "avatar", "is_approved", "user"]
    

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = '__all__'

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'

class StatusOrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatusOrderItem
        fields = '__all__'
    
    def update(self, instance, validated_data):
        print(instance)
        instance.order = validated_data.get('order')
        instance.status = validated_data.get('status')
        instance.save()
        return instance