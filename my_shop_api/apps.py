from django.apps import AppConfig


class MyShopApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'my_shop_api'
