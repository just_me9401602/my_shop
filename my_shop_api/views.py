from django.shortcuts import render
from payments.models import Comment, OrderItem, Order, StatusOrderItem
from django.http import JsonResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from .serializers import CommentSerializer, UserSerializer, ProductSerializer, OrderItemSerializer, OrderSerializer, StatusOrderItemSerializer, ProfileSerializer
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAdminUser, IsAuthenticated
from django.contrib.auth import get_user_model
from my_shop.models import Product
from accounts.models import Profile
from django.contrib.auth.models import Group

class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['order_item']
    ordering = ['created_at']
    permission_classes = [IsAuthenticatedOrReadOnly | IsAdminUser]

class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated | IsAdminUser]

class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['is_approved']
    permission_classes = [IsAuthenticatedOrReadOnly | IsAdminUser]

    def get_queryset(self):
        group = self.kwargs.get('group')
        if group == 'sellers':
            return Profile.objects.filter(user__in=Group.objects.get(name='sellers').user_set.all())
        if group == 'buyers':
            return Profile.objects.filter(user__in=Group.objects.get(name='buyers').user_set.all())
        if group == 'admins':
            return Profile.objects.filter(user__in=Group.objects.get(name='admins').user_set.all())
        return super().get_queryset()
    
    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return super().partial_update(request, *args, **kwargs)

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['seller']
    permission_classes = [IsAuthenticatedOrReadOnly | IsAdminUser]

class OrderItemViewSet(viewsets.ModelViewSet):
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['seller', 'status']
    permission_classes = [IsAuthenticatedOrReadOnly | IsAdminUser]

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        status = request.data.get('status')

        if status == '4': # Отменен
            order_item = OrderItem.objects.get(id=kwargs['pk'])
            product = Product.objects.get(id=order_item.product.id)
            product.quontity += order_item.quontity
            product.save()
        return super().partial_update(request, *args, **kwargs)

class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['buyer']
    permission_classes = [IsAuthenticatedOrReadOnly | IsAdminUser]

class StatusOrderItemViewSet(viewsets.ModelViewSet):
    queryset = StatusOrderItem.objects.all()
    serializer_class = StatusOrderItemSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['status']
    permission_classes = [IsAuthenticatedOrReadOnly | IsAdminUser]
# Create your views here.
