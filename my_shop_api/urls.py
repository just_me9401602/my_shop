from django.urls import path, include
from .views import CommentViewSet, UserViewSet, ProductViewSet, OrderItemViewSet, OrderViewSet, StatusOrderItemViewSet, ProfileViewSet

urlpatterns = [
    path('auth/', include('rest_framework.urls')),
    path('comments/', CommentViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('users/', UserViewSet.as_view({'get': 'list'})),
    path('users/<int:pk>/', UserViewSet.as_view({'get': 'retrieve'})),
    path('products/<int:pk>/', ProductViewSet.as_view({'get': 'retrieve'})),
    path('order_items/', OrderItemViewSet.as_view({'get': 'list'})),
    path('order_items/<int:pk>/', OrderItemViewSet.as_view({'get': 'retrieve', 'patch': 'partial_update'})),
    path('order/<int:pk>/', OrderViewSet.as_view({'get': 'retrieve'})),
    path('status/order_items/', StatusOrderItemViewSet.as_view({'get': 'list'})),
    path('profiles/<str:group>/', ProfileViewSet.as_view({'get': 'list'})),
    path('profiles/approve/<int:pk>/', ProfileViewSet.as_view({'get': 'retrieve', 'patch': 'partial_update'})), 
]
