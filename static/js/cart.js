
function changeSelect(event, id){
    event.preventDefault();
    let select = event.target;
    let qty = $(select).find("option:selected").text();
    console.log('select was change', id, qty);
    $.ajax({
        url: '/cart/update/',
        data: {product_id: id, product_qty: qty, action: 'post',},
        type: 'POST',
        dataType: 'json',
        success: function(response){
            console.log(response.status);
            location.reload();
        },
        error: function(error){
            console.log(error)
        }
    })
};
function addComment(event, id){
  event.preventDefault();
  let button = event.target;
  comment = $('#comment[data-index="'+id+'"]').text();
  $('#commentBodyTextarea').val(comment);
  $('#saveBtn').data('id', id);
//   console.log(id);
//   console.log(comment)
}

function saveComment(event){
    event.preventDefault();
    let button = event.target;
    product_id = $(button).data('id');
    comment = $('#commentBodyTextarea').val();
    console.log(product_id);
    console.log(comment);
    $('#commentModal').modal('hide');
    $.ajax({
        url: '/cart/comment_update/',
        data: {product_id: product_id, comment: comment, action: 'post',},
        type: 'POST',
        dataType: 'json',
        success: function(response){
            console.log(response.status);
            location.reload();
        },
        error: function(error){
            console.log(error)
        }
    })
}



