function commentShow(event, productName, orderItemId){
    event.preventDefault();
    $('#commentTextarea').val('');
    $('.offcanvas-body').empty();
    $('#offcanvasRightLabel').text(productName)
    $('#addComment-btn').data('id', orderItemId);
    $.ajax({
      type: 'GET',
      url: '/api/comments/?order_item='+orderItemId,
      success: function(comments){
          $.each(comments, function(key, comm){
            $.ajax({
              type: 'GET',
              url: '/api/users/'+comm.user,
              success: function(user_obj){
                let username = user_obj.username;
                let alignItems;
                if (user_obj.id=='{{user.id}}'){
                  alignItems='align-items-end';
                }else{
                  alignItems='align-items-start';
                }

                let html = '<div class="d-flex '+alignItems+' flex-column">'+
                  '<div class="card mb-2" style="width: 18rem;">'+
                    '<div class="card-body">'+
                      '<h5 class="card-title">'+username+'</h5>'+
                      '<h6 class="card-subtitle mb-2 text-body-secondary">'+comm.created_at+'</h6>'+
                      '<p class="card-text">'+comm.text+'</p>'+
                    '</div>'+
                  '</div>'+
                '</div>';
                $('.offcanvas-body').append(html);
              },
              error: function(error){
                console.log(error);
              }
            });
          });
      },
      error: function(error){
          console.log(error);
     }
    })
  }; 

  function addComment(event){
    event.preventDefault();
    let text = $('#commentTextarea').val();
    let button = event.target;
    orderItemId = $(button).data('id');
    $.ajax({
      type: 'POST',
      url: '/api/comments/',
      data:{
        user: '{{user.id}}',
        text: $('#commentTextarea').val(),
        order_item: orderItemId,
        csrfmiddlewaretoken: '{{ csrf_token }}',
      },
      dataType: 'json',
      success: function(newComment){
        $.ajax({
          type: 'GET',
          url: '/api/users/'+newComment.user,
          success: function(user_obj){
            let username = user_obj.username;
            let alignItems;
            if (user_obj.id=='{{user.id}}'){
              alignItems='align-items-end';
            }else{
              alignItems='align-items-start';
            }
            let html = '<div class="d-flex '+alignItems+' flex-column">'+
              '<div class="card mb-2" style="width: 18rem;">'+
                '<div class="card-body">'+
                  '<h5 class="card-title">'+username+'</h5>'+
                  '<h6 class="card-subtitle mb-2 text-body-secondary">'+newComment.created_at+'</h6>'+
                  '<p class="card-text">'+newComment.text+'</p>'+
                '</div>'+
              '</div>'+
            '</div>';
            $('.offcanvas-body').append(html);
            
          },
          error: function(error){
            console.log(error);
          }
        });
        $('#commentTextarea').val('');
      },
      error: function(error){
        console.log(error)
      }
    });
  };