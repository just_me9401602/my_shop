$(document).ready(function(){
    $('.fa-heart').click(function(event){
      
      event.preventDefault;
      let button = $(this)
      let id = button.data('id');
      console.log('button was click', id);
      
      $.ajax({
        url: '/favorites/add/',
        data: {product_id: id},
        method: 'POST',
        dataType: 'json',
        success: function(response){
          console.log(response['is_favorite'])
          if (response['is_favorite']===true){
            button.css('color', 'red');
            $('.toast-body').text('Товар добавлен в Избранное');
            $('#liveToast').toast("show");
          }else{
            button.css('color', 'black');
            $('.toast-body').text('Товар удален из Избранного');
            $('#liveToast').toast("show");
          }
        },
        error: function(response){
          console.log(response)
        }
      })
    });
//   });
  
//   $(document).ready(function(){
    $('.fa-cart-shopping').click(function(event){
      event.preventDefault;
      let button = $(this)
      let id = button.data('id');
      console.log('button was click', id);
  
      $.ajax({
        method: 'POST',
        url: "/cart/add/",
        data: {
          product_id: id,
          product_qty: 1,
          action: 'post'
        },
        dataType: 'json',
        success: function(response){
          console.log(response.cart_qty);
          $('.toast-body').text('Товар добавлен в Корзину');
          $('#liveToast').toast("show");
          $('#cartCount').removeClass("invisible");
          $('#cartCount').text(response.cart_qty);
          
        },
        error: function(response){
          console.log(response)
        }
      })
    });
  });  