function favoriteClick(event, id) {
    event.preventDefault;
    let button = event.target;
    console.log('button was click', button);
    $.ajax({
        url: '/favorites/add/',
        data: {product_id: id},
        method: 'POST',
        dataType: 'json',
        success: function(response){
          console.log(response['is_favorite'])
          if (response['is_favorite']===true){
            $(button).css('color', 'red');
            $('.toast-body').text('Товар добавлен в Избранное');
            $('#liveToast').toast("show");
            location.reload()
          }else{
            $(button).css('color', 'black');
            $('.toast-body').text('Товар удален из Избранного');
            $('#liveToast').toast("show");
            location.reload()
          }
        },
        error: function(response){
          console.log(response)
        }
    })
}


function cartClick(event, id) {
    event.preventDefault;
    let button = event.target;
    console.log('button was click', button);
    $.ajax({
    url: '/cart/add/',
    data: {product_id: id, product_qty: 1, action: 'post'},
    method: 'POST',
    dataType: 'json',
    success: function(response){
        console.log(response.cart_qty);
        $('.toast-body').text('Товар добавлен в Корзину');
        $('#liveToast').toast("show");
        $('#cartCount').removeClass("invisible");
        $('#cartCount').text(response.cart_qty);
        
        },
        error: function(response){
        console.log(response)
        }
    })
}
