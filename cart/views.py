from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from cart.cart import Cart
from my_shop.models import Product
from django.http import JsonResponse

def cart_view(request):
    cart = Cart(request)
    context = {
        'cart': cart,
    }
    return render(request, 'cart/cart-view.html', context=context)

@csrf_exempt
def cart_add_view(request):
    cart = Cart(request)

    if request.POST.get('action') == 'post':
        product_id = request.POST.get('product_id')
        product_qty = int(request.POST.get('product_qty'))
        product = get_object_or_404(Product, id=product_id)
        cart.add(product=product, quantity=product_qty)

        cart_qty = cart.__len__()

        return JsonResponse({'status': 'Product added to cart', 'cart_qty': cart_qty, 'product': product.name})

@csrf_exempt
def cart_update_view(request):
    cart = Cart(request)

    if request.POST.get('action') == 'post':
        product_id = int(request.POST.get('product_id'))
        product_qty = int(request.POST.get('product_qty'))
        product = get_object_or_404(Product, id=product_id)
        cart.update(product=product, quantity=product_qty)

        product_price = product.price
        cart_qty = cart.__len__()
        cart_total = cart.get_total_price()

        return JsonResponse({'status': 'Cart updated', 'cart_qty': cart_qty, 'cart_total': cart_total, 'product_price': product_price})    

@csrf_exempt
def comment_update_view(request):
    cart = Cart(request)

    if request.POST.get('action') == 'post':
        product_id = int(request.POST.get('product_id'))
        comment = request.POST.get('comment')
        product = get_object_or_404(Product, id=product_id)
        cart.comment_update(product=product, comment=comment)

        return JsonResponse({'status': 'Comment updated', 'comment': comment})

@csrf_exempt
def cart_delete_view(request):
    cart = Cart(request)
    product_id = request.POST.get('product_id')
    
    cart.delete(product_id=product_id)
    cart_qty = cart.__len__()
    cart_total = cart.get_total_price()

    return JsonResponse({'status': 'Card deleted', 'cart_qty': cart_qty, 'cart_total': cart_total})

@csrf_exempt
def cart_clear_view(request):
    cart = Cart(request)
    cart.clear()
    return JsonResponse({'status': 'Cart cleared'})
# Create your views here.
