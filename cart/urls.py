from django.urls import path
from .views import cart_view, cart_update_view, cart_add_view, cart_delete_view, cart_clear_view, comment_update_view

urlpatterns = [
    path('', cart_view, name='cart_view'),
    path('add/', cart_add_view, name='cart_add'),
    path('update/', cart_update_view, name='cart_update'),
    path('delete/', cart_delete_view, name='cart_delete'),
    path('clear/', cart_clear_view, name='cart_clear'),
    path('comment_update/', comment_update_view, name='cart_comment_update'),
]